
docker run --name waylandgdb01 -d postgres -t waylandgdb01
docker run --name app --link waylandgdb01:db -p 0.0.0.0:80:80 -p 0.0.0.0:443:443 -p 127.0.0.1::22 -d -t waylandgapp
