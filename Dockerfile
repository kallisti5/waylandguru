FROM ubuntu

ENV SITE_NAME waylandguru
ENV APP_PATH /var/www/$SITE_NAME

RUN apt-get update -q

# Install basic server stuff
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qy curl vim wget unzip openssh-server build-essential nodejs

# Install software

# nginx web server
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qy nginx
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

RUN echo "server {" > /etc/nginx/sites-enabled/default
RUN echo "    root $APP_PATH/public;" >> /etc/nginx/sites-enabled/default
RUN echo "    server_name _;" >> /etc/nginx/sites-enabled/default
RUN echo "    location / {" >> /etc/nginx/sites-enabled/default
RUN echo "        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;" >> /etc/nginx/sites-enabled/default
RUN echo "        proxy_redirect off;" >> /etc/nginx/sites-enabled/default
RUN echo "        proxy_set_header Host \$http_host;" >> /etc/nginx/sites-enabled/default
RUN echo "        if (\!-f $request_filename) {" >> /etc/nginx/sites-enabled/default
RUN echo "            proxy_pass http://localhost:8080;" >> /etc/nginx/sites-enabled/default
RUN echo "            break;" >> /etc/nginx/sites-enabled/default
RUN echo "        }" >> /etc/nginx/sites-enabled/default
RUN echo "    }" >> /etc/nginx/sites-enabled/default
RUN echo "}" >> /etc/nginx/sites-enabled/default

# pg client for gem
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qy postgresql-client libpq-dev

# Install rvm, ruby, bundler
RUN curl -sSL https://get.rvm.io | bash -s stable
RUN /bin/bash -l -c "rvm requirements"
RUN /bin/bash -l -c "rvm install 2.1.0"
RUN /bin/bash -l -c "gem install bundler --no-ri --no-rdoc"

RUN /bin/bash -l -c "gem install iconv --no-ri --no-rdoc"
RUN /bin/bash -l -c "gem install rails --no-ri --no-rdoc"
RUN /bin/bash -l -c "gem install pg --no-ri --no-rdoc"
RUN /bin/bash -l -c "gem install radiant --no-ri --no-rdoc"

# Install Site
RUN rm -rf $APP_PATH
RUN mkdir -p $APP_PATH
RUN /bin/bash -l -c "source /etc/profile.d/rvm.sh && rails $APP_PATH"
RUN echo "gem 'casein', '5.0.0'" >> $APP_PATH/Gemfile
RUN chmod 777 -R $APP_PATH # eew

RUN echo "production:" > $APP_PATH/config/database.yml
RUN echo "  adapter: postgresql" >> $APP_PATH/config/database.yml
RUN echo "  encoding: unicode" >> $APP_PATH/config/database.yml
RUN echo "  database: $SITE_NAME" >> $APP_PATH/config/database.yml
RUN echo "  username: postgres" >> $APP_PATH/config/database.yml
RUN echo "  host: $DB_PORT_5432_TCP_ADDR" >> $APP_PATH/config/database.yml
RUN echo "  port: $DB_PORT_5432_TCP_PORT" >> $APP_PATH/config/database.yml

WORKDIR $APP_PATH
RUN /bin/bash -l -c "source /etc/profile.d/rvm.sh && bundle install"
RUN /bin/bash -l -c "source /etc/profile.d/rvm.sh && rails g casein:install"

# Write out entry script
RUN echo "#!/bin/bash" > /entry.sh
RUN echo "cd $APP_PATH" >> /entry.sh
RUN echo "source /etc/profile.d/rvm.sh" >> /entry.sh
RUN echo "rake db:create" >> /entry.sh
RUN echo "rake db:migrate" >> /entry.sh
RUN echo "rake casein:users:create_admin email=kallisti5@unixzen.com" >> /entry.sh
RUN echo "bundle exec unicorn -D -p 8080" >> /entry.sh
RUN echo "nginx" >> /entry.sh
RUN chmod 755 /entry.sh

EXPOSE 80

ENTRYPOINT /entry.sh

RUN DEBIAN_FRONTEND=noninteractive apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
